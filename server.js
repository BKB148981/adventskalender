var bodyparser = require('body-parser')
var mysql = require('mysql')

var con = mysql.createConnection({
    host: "130.255.124.99",
    user: "weihnachtsmann",
    password: "nikolaus",
    database: "db2412"
})

con.connect(function(err){
    con.query("CREATE TABLE IF NOT EXISTS kalendereintraege(tag INT AUTO_INCREMENT, eintrag VARCHAR(500), PRIMARY KEY(tag));", function(err, result){

    })
})

const express = require('express')

const app = express()

app.set('view engine', 'ejs')

app.use(bodyparser.urlencoded({extended: true}))

app.use(express.static('public'))

const server = app.listen(process.env.PORT || 3000, () => {
    console.log('Server lauscht auf Port %s', server.address().port)
})

app.get('/',function(req,res){
    con.query("SELECT COUNT(tag) FROM kalendereintraege;", function(err,result){

        let button = ""
        let tbxStyle = ""

        let anzahlEintraege = result[0][Object.keys(result[0])[0]]

        console.log("Anzahl der Kalendereinträge: " + anzahlEintraege)

        if(anzahlEintraege < 24){
            button = "Absenden!"
        }else{
            button = "Türchen Nr. " + new Date().getDate() + " öffnen" 
            tbxStyle = "display:none"
        }

        res.render('index.ejs',{
            button:button,
            tbxStyle:tbxStyle,
            lblStyle: "display:none",
            lblKalenderspruch: ""
        })
    })
})

app.post('/', function(req,res){
    con.query("SELECT COUNT(tag) FROM kalendereintraege;", function(err,result){
        
        let anzahlEintraege = result[0][Object.keys(result[0])[0]]

        if(anzahlEintraege < 24){
            if(req.body.text != ""){
                con.query("INSERT INTO kalendereintraege(eintrag) VALUES ('" + req.body.text + "');", function(err, result){
                    res.render('index.ejs', {
                        button: "Eintrag angelegt",
                        lblKalenderspruch:"",
                        tbxStyle:"display:none",
                        lblStyle:""
                    })                    
                })
            }else{
                res.render('index.ejs', {
                    button: "Der Eintrag darf nicht leer sein.",
                    lblKalenderspruch:"",
                    tbxStyle:"",
                    lblStyle:"display:none"
                })                    
            }
        }
        if(anzahlEintraege >= 24){
            if(new Date().getMonth() == 11 && new Date().getDate() <= 24 ){
                con.query("SELECT eintrag from kalendereintraege WHERE tag = '" + new Date().getDate() + "';", function(err, result) {
                    res.render('index.ejs', {
                        button: "Noch " + (24 - new Date().getUTCDate()) + " Tage bis Weihnachten",
                        lblKalenderspruch:result[0][Object.keys(result[0])[0]],
                        tbxStyle:"display:none",
                        lblStyle:""
                    })                           
                })
            }
        }
    })
})
